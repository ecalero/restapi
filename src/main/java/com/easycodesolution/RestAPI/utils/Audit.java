package com.easycodesolution.RestAPI.utils;

import lombok.Data;

import javax.persistence.MappedSuperclass;
import java.sql.Timestamp;

@Data
@MappedSuperclass
public class Audit {

    private int createdBy;
    private int updatedBy;
    private Timestamp createdAt;
    private Timestamp updatedAt;
}
