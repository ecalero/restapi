package com.easycodesolution.RestAPI.repository;

import com.easycodesolution.RestAPI.entities.Person;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PersonRepository extends CrudRepository<Person, Integer> {

    List<Person> findPersonByName(String name);
    List<Person> findAll();
}
