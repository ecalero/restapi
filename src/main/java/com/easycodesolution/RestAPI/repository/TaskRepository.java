package com.easycodesolution.RestAPI.repository;

import com.easycodesolution.RestAPI.entities.Task;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TaskRepository extends CrudRepository<Task, Integer> {
    List<Task> findAll();
    List<Task> findTasksByUserId(int userId);
}
