package com.easycodesolution.RestAPI.repository;

import com.easycodesolution.RestAPI.entities.Project;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProjectRepository extends CrudRepository<Project, Integer> {

    public List<Project> findProjectByName(String name);
    public List<Project> findAll();
}
