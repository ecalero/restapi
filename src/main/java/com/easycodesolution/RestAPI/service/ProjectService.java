package com.easycodesolution.RestAPI.service;

import com.easycodesolution.RestAPI.entities.Project;
import com.easycodesolution.RestAPI.repository.ProjectRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProjectService {

    private final ProjectRepository projectRepository;

    public Project saveOrUpdateProject(Project project) {
        return projectRepository.save(project);
    }

    public List<Project> getAllProjects() {
        return  projectRepository.findAll();
    }

}
