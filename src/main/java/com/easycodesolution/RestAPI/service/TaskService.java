package com.easycodesolution.RestAPI.service;

import com.easycodesolution.RestAPI.entities.Task;
import com.easycodesolution.RestAPI.repository.TaskRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TaskService {

    private final TaskRepository taskRepository;


    public Task saveOrUpdateTask(Task task) {
        return taskRepository.save(task);
    }

    public List<Task> getAllTask() {
        return taskRepository.findAll();
    }

    public List<Task> getTaskByUserId(int userId) {
        return taskRepository.findTasksByUserId(userId);
    }
}
