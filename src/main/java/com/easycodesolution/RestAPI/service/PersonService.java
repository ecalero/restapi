package com.easycodesolution.RestAPI.service;

import com.easycodesolution.RestAPI.entities.Person;
import com.easycodesolution.RestAPI.repository.PersonRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PersonService {

    private final PersonRepository personRepository;

    public Person saveOrUpdatePerson(Person person) {
        return personRepository.save(person);
    }

    public List<Person> getAllPersons() {
        return personRepository.findAll();
    }

    public List<Person> getPersonByName(String name) {
        return personRepository.findPersonByName(name);
    }
}
