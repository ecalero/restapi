package com.easycodesolution.RestAPI.controller;

import com.easycodesolution.RestAPI.entities.Person;
import com.easycodesolution.RestAPI.service.PersonService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/person")
public class PersonController {

    private final PersonService personService;
    @PostMapping
    public ResponseEntity<Person> saveOrUpdatePerson(@RequestBody Person person) {
        try{
            return ResponseEntity.ok(personService.saveOrUpdatePerson(person));
        }catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping
    public ResponseEntity<List<Person>> getAllPerson() {
        try{
            return ResponseEntity.ok(personService.getAllPersons());
        }catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping(value = "/{name}")
    public ResponseEntity<List<Person>> getAllPersonByName(@PathVariable String name) {
        try{
            return ResponseEntity.ok(personService.getPersonByName(name));
        }catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
