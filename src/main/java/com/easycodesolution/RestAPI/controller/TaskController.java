package com.easycodesolution.RestAPI.controller;

import com.easycodesolution.RestAPI.entities.Person;
import com.easycodesolution.RestAPI.entities.Task;
import com.easycodesolution.RestAPI.service.TaskService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/task")
public class TaskController {

    private final TaskService taskService;

    @PostMapping
    public ResponseEntity<Task> saveOrUpdateTask(@RequestBody Task task) {
        try{
            return ResponseEntity.ok(taskService.saveOrUpdateTask(task));
        }catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping
    public ResponseEntity<List<Task>> getAllTask() {
        try{
            return ResponseEntity.ok(taskService.getAllTask());
        }catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping(value = "/{userId}")
    public ResponseEntity<List<Task>> getTaskByUserId(@PathVariable int userId) {
        try{
            return ResponseEntity.ok(taskService.getTaskByUserId(userId));
        }catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
