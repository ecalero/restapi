package com.easycodesolution.RestAPI.controller;


import com.easycodesolution.RestAPI.entities.Project;
import com.easycodesolution.RestAPI.service.ProjectService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/project")
public class ProjectController {

    private final ProjectService projectService;

    @PostMapping
    public ResponseEntity<Project> saveOrUpdateProject(@RequestBody Project project) {
        try{
            return ResponseEntity.ok(projectService.saveOrUpdateProject(project));
        }catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping
    public ResponseEntity<List<Project>> getAllProjects() {
        try{
            return ResponseEntity.ok(projectService.getAllProjects());
        }catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
