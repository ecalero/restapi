package com.easycodesolution.RestAPI.entities;

import com.easycodesolution.RestAPI.utils.Audit;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "task")
public class Task extends Audit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne
    @JoinColumn(name = "project_Id")
    private Project project;
    private int userId;
    private String codeTask;
    private String name;
    private String description;
    private Date assignedDate;
    private double timeEstimate;
    private double timeSpent;
    private int status;
}
