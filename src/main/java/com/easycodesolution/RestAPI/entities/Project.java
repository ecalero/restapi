package com.easycodesolution.RestAPI.entities;

import com.easycodesolution.RestAPI.utils.Audit;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "project")
public class Project extends Audit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String description;
    private int status;

    @OneToMany(mappedBy = "project")
    private List<Task> tasks;
}
