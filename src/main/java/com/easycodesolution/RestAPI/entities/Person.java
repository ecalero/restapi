package com.easycodesolution.RestAPI.entities;

import com.easycodesolution.RestAPI.utils.Audit;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "person")
public class Person extends Audit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String lastName;
    private Date birthday;
    private String docId;
    private int status;
}
