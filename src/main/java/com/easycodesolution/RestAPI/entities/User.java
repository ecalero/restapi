package com.easycodesolution.RestAPI.entities;

import com.easycodesolution.RestAPI.utils.Audit;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "user")
public class User extends Audit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int personId;
    private String userName;
    private String password;
    private int status;
}
